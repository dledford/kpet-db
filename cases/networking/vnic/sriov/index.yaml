name: 'Networking vnic sriov'
universal_id: redhat_vnic_sriov
maintainers:
  - name: Zhiqiang Fang
    email: zfang@redhat.com
    gitlab: zfang1
  - name: Jiying Qiu
    email: jiqiu@redhat.com
    gitlab: jiqiu
  - name: Rick Alongi
    email: ralongi@redhat.com
    gitlab: ralongi
  - name: Hekai Wang
    email: hewang@redhat.com
    gitlab: wanghekai
origin: suites_zip
location: networking/vnic/sriov
enabled:
  components:
    - not: debugbuild
  trees:
    or:
      # disabling on rhel9|c9s as the current test frequency upstream and
      # with total test execution of about 10 hours
      # is causing MR for rhel9|c9s to be blocked for days.
      # - rhel9|c9s
      - rawhide
  arches:
    - x86_64
target_sources:
  - drivers/net/ethernet/intel/ice/.*
  - drivers/net/ethernet/intel/iavf/.*
sets:
  - net-ice
max_duration_seconds: 3600
environment:
  NIC_DRIVER: "ice"
  SRIOV_SKIP_SETUP_ENV: "no"
  SET_DMESG_CHECK_KEY: "yes"
  NAY: "yes"
  image_name: "rhel9.1.qcow2"

cases:
  pf_remote:
    name: "pf_remote"
    environment:
      SRIOV_TOPO: "sriov_test_pf_remote"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  pf_remote_jumbo:
    name: "pf_remote_jumbo"
    environment:
      SRIOV_TOPO: "sriov_test_pf_remote_jumbo"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  # all tests are failing due to same issue:
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1532
  # disable some of them, so pipeline can run faster...
  # pf_vlan_remote:
  #   name: "pf_vlan_remote"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_pf_vlan_remote"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # vf_remote:
  #   name: "vf_remote"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_vf_remote"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # vf_remote_switchdev:
  #   name: "vf_remote_switchdev"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_vf_remote_switchdev"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  vf_remote_jumbo:
    name: "vf_remote_jumbo"
    environment:
      SRIOV_TOPO: "sriov_test_vf_remote_jumbo"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 7200
    waived: true

  vf_vlan_remote:
    name: "vf_vlan_remote"
    environment:
      SRIOV_TOPO: "sriov_test_vf_vlan_remote"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 7200

  # vmvf_remote:
  #   name: "vmvf_remote"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_vmvf_remote"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # 1733181_vmvf_remote_pfdown:
  #   name: "1733181_vmvf_remote_pfdown"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_1733181_vmvf_remote_pfdown"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  vmvf_remote_jumbo:
    name: "vmvf_remote_jumbo"
    environment:
      SRIOV_TOPO: "sriov_test_vmvf_remote_jumbo"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 7200
    waived: true

  # vmvf_vlan_remote:
  #   name: "vmvf_vlan_remote"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_vmvf_vlan_remote"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # vmvf_vlan_remote_jumbo:
  #   name: "vmvf_vlan_remote_jumbo"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_vmvf_vlan_remote_jumbo"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # vmvf_vlan_remote_1:
  #   name: "vmvf_vlan_remote_1"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_vmvf_vlan_remote_1"
  #     SKIP_DRIVER: "cxgb4"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # pf_vmvf:
  #   name: "pf_vmvf"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_pf_vmvf"
  #     SKIP_DRIVER: "be2net"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # pf_vmvf_vlan:
  #   name: "pf_vmvf_vlan"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_pf_vmvf_vlan"
  #     SKIP_DRIVER: "be2net"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  vmvf_vmvf:
    name: "vmvf_vmvf"
    environment:
      SRIOV_TOPO: "sriov_test_vmvf_vmvf"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 7200

  vmvf_vmvf_vlan:
    name: "vmvf_vmvf_vlan"
    environment:
      SRIOV_TOPO: "sriov_test_vmvf_vmvf_vlan"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 7200

  # vmvf_bond_remote:
  #   name: "vmvf_bond_remote"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_vmvf_bond_remote"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # vmvf1_vmvf2_remote:
  #   name: "vmvf1_vmvf2_remote"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_vmvf1_vmvf2_remote"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # vmvf1_vmvf2_vlan_remote:
  #   name: "vmvf1_vmvf2_vlan_remote"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_vmvf1_vmvf2_vlan_remote"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # bz1212361:
  #   name: "bz1212361"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_bz1212361"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # bz1205903:
  #   name: "bz1205903"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_bz1205903"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # trusted_vf_override_macaddr_via_bonding:
  #   name: "trusted_vf_override_macaddr_via_bonding"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_trusted_vf_override_macaddr_via_bonding"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  bond_failovermac0:
    name: "bond_failovermac0"
    environment:
      SRIOV_TOPO: "sriov_test_bond_failovermac0"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 3600
    waived: true

  bond_failovermac1:
    name: "bond_failovermac1"
    environment:
      SRIOV_TOPO: "sriov_test_bond_failovermac1"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 3600
    waived: true

  bond_failovermac2:
    name: "bond_failovermac2"
    environment:
      SRIOV_TOPO: "sriov_test_bond_failovermac2"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 3600
    waived: true

  bond_failovermac0_vlan:
    name: "bond_failovermac0_vlan"
    environment:
      SRIOV_TOPO: "sriov_test_bond_failovermac0_vlan"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 3600
    waived: true

  bond_failovermac1_vlan:
    name: "bond_failovermac1_vlan"
    environment:
      SRIOV_TOPO: "sriov_test_bond_failovermac1_vlan"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 3600
    waived: true

  bond_failovermac2_vlan:
    name: "bond_failovermac2_vlan"
    environment:
      SRIOV_TOPO: "sriov_test_bond_failovermac2_vlan"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 3600
    waived: true

  bond_failovermac1_pf_down:
    name: "bond_failovermac1_pf_down"
    environment:
      SRIOV_TOPO: "sriov_test_bond_failovermac1_pf_down"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 3600
    waived: true

  # bond_failovermac2_swport_down:
  #   name: "bond_failovermac2_swport_down"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_bond_failovermac2_swport_down"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # bz1701191:
  #   name: "bz1701191"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_bz1701191"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # bz1392128:
  #   name: "bz1392128"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_bz1392128"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # trusted_vf_allmulticast:
  #   name: "trusted_vf_allmulticast"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_trusted_vf_allmulticast"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # vf_trust_broadcast:
  #   name: "vf_trust_broadcast"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_vf_trust_broadcast"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  max_vfs:
    name: "max_vfs"
    environment:
      SRIOV_TOPO: "sriov_test_max_vfs"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 7200
    waived: true

  # vmvf_vlan_offload_remote:
  #   name: "vmvf_vlan_offload_remote"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_vmvf_vlan_offload_remote"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  #
  # trusted_vf_ipv6addr:
  #   name: "trusted_vf_ipv6addr"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_trusted_vf_ipv6addr"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # end of section disabling tests due to
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1532
  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
  # max_vfs_attaching_to_different_vms:
  #   name: "max_vfs_attaching_to_different_vms"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_max_vfs_attaching_to_different_vms"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  vmvf_different_vlan:
    name: "vmvf_different_vlan"
    environment:
      SRIOV_TOPO: "sriov_test_vmvf_different_vlan"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  bz1441909:
    name: "bz1441909"
    environment:
      SRIOV_TOPO: "sriov_test_bz1441909"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  # disable the failed tests that need to be reviewed, will enable after fix
  # bond_lacp:
  #  name: "bond_lacp"
  #  environment:
  #    SRIOV_TOPO: "sriov_test_bond_lacp"
  #  cases:
  #    cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  trusted_vf_promisc:
    name: "trusted_vf_promisc"
    environment:
      SRIOV_TOPO: "sriov_test_trusted_vf_promisc"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 3600
    waived: true

  trusted_vf_promisc_vlan:
    name: "trusted_vf_promisc_vlan"
    environment:
      SRIOV_TOPO: "sriov_test_trusted_vf_promisc_vlan"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 3600
    waived: true

  bz1445814:
    name: "bz1445814"
    environment:
      SRIOV_TOPO: "sriov_test_bz1445814"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  # comment out this test as it has been moved to
  # networking/nic/functions/consistent_interface_name
  #
  # vfname:
  #  name: "vfname"
  #  environment:
  #    SRIOV_TOPO: "sriov_test_vfname"
  #  cases:
  #    cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  hostdev_vmvf_remote:
    name: "hostdev_vmvf_remote"
    environment:
      SRIOV_TOPO: "sriov_test_hostdev_vmvf_remote"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  vmvf1vf2_remote:
    name: "vmvf1vf2_remote"
    environment:
      SRIOV_TOPO: "sriov_test_vmvf1vf2_remote"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  vmvf1vf2_same_pf_remote:
    name: "vmvf1vf2_same_pf_remote"
    environment:
      SRIOV_TOPO: "sriov_test_vmvf1vf2_same_pf_remote"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  spoofchk:
    name: "spoofchk"
    environment:
      SRIOV_TOPO: "sriov_test_spoofchk"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 3600
    waived: true

  bz1493953:
    name: "bz1493953"
    environment:
      SRIOV_TOPO: "sriov_test_bz1493953"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  bz1483396:
    name: "bz1483396"
    environment:
      SRIOV_TOPO: "sriov_test_bz1483396"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  bz1794812_excessive_interrupts:
    name: "bz1794812_excessive_interrupts"
    environment:
      SRIOV_TOPO: "sriov_test_bz1794812_excessive_interrupts"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  # disable the failed tests that need to be reviewed, will enable after fix
  # vmvf_multicast:
  #  name: "vmvf_multicast"
  #  environment:
  #    SRIOV_TOPO: "sriov_test_vmvf_multicast"
  #  cases:
  #    cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  vmpf_vmvf_remote:
    name: "vmpf_vmvf_remote"
    environment:
      SRIOV_TOPO: "sriov_test_vmpf_vmvf_remote"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  # disable the failed tests that need to be reviewed, will enable after fix
  # vmpfbond_vmvfbond_remote:
  #  name: "vmpfbond_vmvfbond_remote"
  #  environment:
  #    SRIOV_TOPO: "sriov_test_vmpfbond_vmvfbond_remote"
  #  cases:
  #    cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disable the failed tests that need to be reviewed, will enable after fix
  # attach_method_is_forward_hostdev:
  #  name: "attach_method_is_forward_hostdev"
  #  environment:
  #    SRIOV_TOPO: "sriov_test_attach_method_is_forward_hostdev"
  #  cases:
  #    cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disable the failed tests that need to be reviewed, will enable after fix
  # attach_method_is_forward_hostdev_vlan:
  #  name: "attach_method_is_forward_hostdev_vlan"
  #  environment:
  #    SRIOV_TOPO: "sriov_test_attach_method_is_forward_hostdev_vlan"
  #  cases:
  #    cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  vmvf_max_tx_rate:
    name: "vmvf_max_tx_rate"
    environment:
      SRIOV_TOPO: "sriov_test_vmvf_max_tx_rate"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  create_remove_vfs_memoryleak:
    name: "create_remove_vfs_memoryleak"
    environment:
      SRIOV_TOPO: "sriov_test_create_remove_vfs_memoryleak"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  # disable the failed tests that need to be reviewed, will enable after fix
  # vmvf_reg_ureg_multicast_addr:
  #  name: "vmvf_reg_ureg_multicast_addr"
  #  environment:
  #    SRIOV_TOPO: "sriov_test_vmvf_reg_ureg_multicast_addr"
  #  cases:
  #    cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disable the failed tests that need to be reviewed, will enable after fix
  # vf_creation:
  #  name: "vf_creation"
  #  environment:
  #    SRIOV_TOPO: "sriov_test_vf_creation"
  #  cases:
  #    cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  vf_vlan_negative:
    name: "vf_vlan_negative"
    environment:
      SRIOV_TOPO: "sriov_test_vf_vlan_negative"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  vf_pf_speed_consistency:
    name: "vf_pf_speed_consistency"
    environment:
      SRIOV_TOPO: "sriov_test_vf_pf_speed_consistency"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  #  vmvf_remote_switchdev:
  #    name: "vmvf_remote_switchdev"
  #    environment:
  #      SRIOV_TOPO: "sriov_test_vmvf_remote_switchdev"
  #    cases:
  #      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  #  vf_mac_switchdev_bz1814350:
  #    name: "vf_mac_switchdev_bz1814350"
  #    environment:
  #      SRIOV_TOPO: "sriov_test_vf_mac_switchdev_bz1814350"
  #    cases:
  #      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  #  switchdev_bz1870593:
  #    name: "switchdev_bz1870593"
  #    environment:
  #      SRIOV_TOPO: "sriov_test_switchdev_bz1870593"
  #    cases:
  #      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  #  vf_remote_jumbo_switchdev:
  #    name: "vf_remote_jumbo_switchdev"
  #    environment:
  #      SRIOV_TOPO: "sriov_test_vf_remote_jumbo_switchdev"
  #    cases:
  #      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  #  vmvf_remote_jumbo_switchdev:
  #    name: "vmvf_remote_jumbo_switchdev"
  #    environment:
  #      SRIOV_TOPO: "sriov_test_vmvf_remote_jumbo_switchdev"
  #    cases:
  #      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  cntvf_cntvf:
    name: "cntvf_cntvf"
    environment:
      SRIOV_TOPO: "sriov_test_cntvf_cntvf"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  cntvf_cntvf_vlan:
    name: "cntvf_cntvf_vlan"
    environment:
      SRIOV_TOPO: "sriov_test_cntvf_cntvf_vlan"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  cntvf_cntvf_jumbo:
    name: "cntvf_cntvf_jumbo"
    environment:
      SRIOV_TOPO: "sriov_test_cntvf_cntvf_jumbo"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 7200

  podcntvf_podcntvf:
    name: "podcntvf_podcntvf"
    environment:
      SRIOV_TOPO: "sriov_test_podcntvf_podcntvf"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    waived: true

  podcntvf1_podcntvf2:
    name: "podcntvf1_podcntvf2"
    environment:
      SRIOV_TOPO: "sriov_test_podcntvf1_podcntvf2"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    waived: true

  podcntvf_remote:
    name: "podcntvf_remote"
    environment:
      SRIOV_TOPO: "sriov_test_podcntvf_remote"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    waived: true

  #  reproduce_2021326_reboot_check:
  #    name: "reproduce_2021326_reboot_check"
  #    environment:
  #      SRIOV_TOPO: "sriov_test_reproduce_2021326_reboot_check"
  #    cases:
  #      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disable the failed tests that need to be reviewed, will enable after fix
  # podcnts_vfs_remote_bz2088787:
  #  name: "podcnts_vfs_remote_bz2088787"
  #  environment:
  #    SRIOV_TOPO: "sriov_test_podcnts_vfs_remote_bz2088787"
  #  cases:
  #    cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467

  cntvf_reboot:
    name: "cntvf_reboot"
    environment:
      SRIOV_TOPO: "sriov_test_cntvf_reboot"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 9000
    waived: true
  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
  # delete_vm_with_kernel_args:
  #   name: "delete_vm_with_kernel_args"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_delete_vm_with_kernel_args"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
  # negative_create_vfs_2000180:
  #   name: "negative_create_vfs_2000180"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_negative_create_vfs_2000180"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
  # reproduce_2021326:
  #   name: "reproduce_2021326"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_reproduce_2021326"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
  # vf_cfg_stress:
  #   name: "vf_cfg_stress"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_vf_cfg_stress"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disable the failed tests that need to be reviewed, will enable after fix
  # bond_mode2:
  #  name: "bond_mode2"
  #  environment:
  #    SRIOV_TOPO: "sriov_test_bond_mode2"
  #  cases:
  #    cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disable the failed tests that need to be reviewed, will enable after fix
  # bond_mode2_vlan:
  #  name: "bond_mode2_vlan"
  #  environment:
  #    SRIOV_TOPO: "sriov_test_bond_mode2_vlan"
  #  cases:
  #    cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
  # 2049237_VF_mac_reset_zero:
  #   name: "2049237_VF_mac_reset_zero"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_2049237_VF_mac_reset_zero"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
  # 2049237_VF_mac_reset_zero_reboot_check:
  #   name: "2049237_VF_mac_reset_zero_reboot_check"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_2049237_VF_mac_reset_zero_reboot_check"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
  # bz2057244_vf_not_up:
  #   name: "bz2057244_vf_not_up"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_bz2057244_vf_not_up"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
  # bz2070917_vlan_bridge_nic:
  #   name: "bz2070917_vlan_bridge_nic"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_bz2070917_vlan_bridge_nic"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
  # bz2055446_no_arp_reply:
  #   name: "bz2055446_no_arp_reply"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_bz2055446_no_arp_reply"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
  # bz2071027_double_tagging:
  #   name: "bz2071027_double_tagging"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_bz2071027_double_tagging"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
  # bz2008373:
  #   name: "bz2008373"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_bz2008373"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  vf_intf_garp_check:
    name: "vf_intf_garp_check"
    environment:
      SRIOV_TOPO: "vf_intf_garp_check"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 3600
    waived: true

  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
  # link_down_on_close:
  #   name: "link_down_on_close"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_link_down_on_close"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
  # reproduce_2000180:
  #   name: "reproduce_2000180"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_reproduce_2000180"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467

  vlan_qinq_baisc:
    name: "vlan_qinq_baisc"
    environment:
      SRIOV_TOPO: "sriov_test_vlan_qinq_baisc"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    waived: true

  bz2080033_re_assign_MACs_to_VFs:
    name: "bz2080033_re_assign_MACs_to_VFs"
    environment:
      SRIOV_TOPO: "sriov_test_bz2080033_re_assign_MACs_to_VFs"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 7200

  # disabling as the test is hanging and aborting with time out
  # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
  # bug_reproducer_2103801:
  #   name: "bug_reproducer_2103801"
  #   environment:
  #     SRIOV_TOPO: "sriov_test_bug_reproducer_2103801"
  #   cases:
  #     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml

  spoofchk_vlan:
    name: "spoofchk_vlan"
    environment:
      SRIOV_TOPO: "sriov_test_spoofchk_vlan"
      SRIOV_USE_HOSTDEV: "yes"
    cases:
      cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
    max_duration_seconds: 3600
    waived: true

# disable the failed tests that need to be reviewed, will enable after fix
# vmvf_connectivity_remain:
#  name: "vmvf_connectivity_remain"
#  environment:
#    SRIOV_TOPO: "sriov_test_vmvf_connectivity_remain"
#  cases:
#    cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
# disabling as the test is hanging and aborting with time out
# https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
# vf_not_created_bz1875338:
#   name: "vf_not_created_bz1875338"
#   environment:
#     SRIOV_TOPO: "sriov_test_vf_not_created_bz1875338"
#   cases:
#     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
# disabling as the test is hanging and aborting with time out
# https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1467
# vf_iface_not_created:
#   name: "vf_iface_not_created"
#   environment:
#     SRIOV_TOPO: "sriov_test_vf_iface_not_created"
#   cases:
#     cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
# disable the failed tests that need to be reviewed, will enable after fix
# vmvf_hibernation:
#  name: "vmvf_hibernation"
#  environment:
#    SRIOV_TOPO: "sriov_test_vmvf_hibernation"
#  cases:
#    cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
# disable the failed tests that need to be reviewed, will enable after fix
# pf_steering_switchdev_bz1856660:
#  name: "pf_steering_switchdev_bz1856660"
#  environment:
#    SRIOV_TOPO: "sriov_test_pf_steering_switchdev_bz1856660"
#  cases:
#    cli_srv: cases/networking/vnic/sriov/cli_srv.yaml
# waived: true
